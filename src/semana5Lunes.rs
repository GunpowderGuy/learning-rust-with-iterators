mod input;

fn consecutivos_a_x(x : usize, fin : usize) -> String {
    (x+1..x+fin+1).fold(String::new(), |acc, arg| acc + &arg.to_string() + "\n")
}

fn pares_mayores_cero(fin : usize) -> String {
    (1..fin*2+1).filter(|x|x % 2 == 0).fold(String::new(), |acc, arg| acc + &arg.to_string() + "\n")
}

fn main() {
 // let a = consecutivos_a_x(1,input::entero(1));
 //let a = consecutivos_a_x(input::entero(1), input::entero(1));
let a = pares_mayores_cero(input::entero(1));
println!("{}", a);
}
