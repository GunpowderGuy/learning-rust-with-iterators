mod input;

fn blackjack(cadena : String) -> bool {
   cadena.len() - cadena.replace("A","").len() >= 4
}

fn pocker(cadena : String) -> String {
    if blackjack(cadena) == true {
        return "Pocker".to_string();
    }
    else {
        return "No Pocker".to_string();
    }
}

fn spell_it(cadena :String) -> String {
   cadena.replace("", "\n")
}

fn consecutivos(lista : Vec<usize>) -> bool {
   (lista[0]..lista[lista.len()+1]).collect::<Vec<usize>>() == lista
}

fn main() {
    let a = pocker(input::cadena("ingrese cadena "));
    println!("{}",a);
    println!("{}",spell_it(input::cadena("ingrese cadena")));
    let lista = input::cadena("ingrese cadena ").chars().map(|x|x.to_digit(9));
    println!("{}",consecutivos(lista));
}
